module.exports = {
    src : {
        root	: 'src/',
        css	: 'src/css/',
        js	  : 'src/js/'
    },

    dest:{
        root	: 'build/',
        css	 : 'build/css/',
        html	: 'build/',
        js	  : 'build/js/'
    }
};