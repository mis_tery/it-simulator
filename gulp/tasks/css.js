var gulp = require('gulp');
    autoprefixer = require('gulp-autoprefixer'),
    config = require('../config'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    cleanCSS = require('gulp-clean-css'),
    importCss = require('gulp-import-css');

gulp.task('css', function () {
    gulp.src(config.src.css+'*.css')
        .pipe(importCss())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(config.dest.css))
        .pipe(reload({stream: true}));
});
gulp.task('css-minify', function () {
    gulp.src(config.src.css+'*.css')
        .pipe(importCss())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(config.dest.css));
});
gulp.task('css:watch', function() {
    gulp.watch(config.src.css+'*', ['css']);
});