var gulp = require('gulp'),
    include = require("gulp-include"),
    config = require('../config'),
    notify = require('gulp-notify'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    babel = require('gulp-babel');

gulp.task('js', function () {
    gulp.src(config.src.js+'**/*.js')
        .pipe(include())
        .on('error', function() {
            notify("Javascript include error");
        })
        .pipe(gulp.dest(config.dest.js))
        .pipe(reload({stream: true}));
});
gulp.task('js-minify', function () {
    gulp.src(config.src.js+'**/*.js')
        .pipe(include())
        .on('error', function() {
            notify("Javascript include error");
        })
        .pipe(babel({
        	minified: true
        }))
        .pipe(gulp.dest(config.dest.js))
        .pipe(reload({stream: true}));
});

gulp.task('js:watch', function() {
    gulp.watch(config.src.js+'*', ['js']);
});