var gulp = require('gulp'),
    config = require('../config');

// copy static files
gulp.task('copy', function() {
    gulp.src(config.src.root+'*.(!html)*')
        .pipe(gulp.dest(config.dest.root));
    gulp.src('css/*.*')
        .pipe(gulp.dest(config.dest.root + 'css-sourse/'));
    gulp.src('src/i/*.*')
        .pipe(gulp.dest(config.dest.root + 'i/'));
});

