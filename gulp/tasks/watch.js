var gulp = require('gulp'),
    gulpSequence = require('gulp-sequence'),
    config = require('../config'),
    del = require('del');

gulp.task('clean', function() {
    return del( 'build' );
});

gulp.task('watch', [
    'html:watch',
    'js:watch',
    'css:watch'
]);

gulp.task('default', function (cb) {
    gulpSequence(
        ['clean'],
        ['html', 'copy', 'js', 'css'],
        ['server', 'watch'])(cb)
});
gulp.task('build', ['html', 'copy','js', 'css'], function() {});
gulp.task('prod', function (cb) {
    gulpSequence(
        ['clean'],
        ['html', 'copy', 'js-minify', 'css-minify'])(cb)
});