$(function() {

    $('.photos-list').slick({
        vertical: false,
        autoplay: true,
        arrows: false,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        swipe: true,
        touchMove: false,
        speed: 1000,
        autoplaySpeed: 1500,
        pauseOnHover: true,
        mobileFirst: true,
        infinite: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 1279,
                settings: {
                    slidesToShow: 8,
                    vertical: true
                }
            }
        ]
    });

    $(".fancybox").fancybox();
});