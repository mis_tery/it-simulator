document.getElementById('show-result').addEventListener( 'click', function() {
	document.getElementById('exam-result').classList.toggle('show');
});

[].slice.call( document.querySelectorAll( '.js-hide-result' ) ).forEach( function( el, i ) {
	el.addEventListener( 'click', function() {
		document.getElementById('exam-result').classList.remove('show');
	});
});
document.getElementById('toogleTaskSidebar').addEventListener( 'click', function() {
	document.getElementById('exam-content').classList.toggle('taskSidebarHidden');
});
