//=include ../../bower_components/codemirror/lib/codemirror.js
//=include ../../bower_components/codemirror/mode/clike/clike.js
//=include ../../bower_components/codemirror/addon/scroll/simplescrollbars.js

var editor = CodeMirror.fromTextArea( document.getElementById( 'editor' ), {
    lineNumbers: true,
    mode: 'text/x-java',
    theme: 'blackboard',
    scrollbarStyle: "overlay"
});